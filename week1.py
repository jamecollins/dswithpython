import sys,os,random
from collections import Counter
#Question 1:
def Question1():
	user_input=input("Enter your string: ")
	list_input=user_input.split(",")
	even_list=[]
	odd_list=[]
	for x in list_input:
		if int(x) % 2 == 0:
			even_list.append(x)
		else:
			odd_list.append(x)
	print("Even list ", end='')
	print(even_list)
	print("Odd list ", end='')
	print(odd_list)

#Question 2:
def Question2():
	l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]
	print(l)

#Question 3:
def Question3():
	s = 'the output that tells you if the object is the type you think or not'
	first_list= s.split(" ")
	first_list=sorted(first_list)
	print(' '.join(first_list))
	print(s.title())

#Question 4:
def Question4():
	n=30
	my_randoms=[]
	for x in range(0,n):
		my_randoms.append(random.randint(0,10))
	print(my_randoms)
	print(Counter(my_randoms))
	for x in range(0,11):
		print("Number {0} => {1} times".format(x,my_randoms.count(x)))

def Main():
	# Question1()
	# Question3()
	Question4()
if __name__ =='__main__':
	Main()